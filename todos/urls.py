from django.urls import path
from todos.views import (
    todo_list,
    show_todo,
    create_list,
    update_list,
    delete_list,
    item_create,
    item_update,
)


urlpatterns = [
    path("", todo_list, name="todo_list_list"),
    path("<int:id>/", show_todo, name="todo_list_detail"),
    path("create/", create_list, name="todo_list_create"),
    path("<int:id>/edit/", update_list, name="todo_list_update"),
    path("<int:id>/delete/", delete_list, name="todo_list_delete"),
    path("items/create/", item_create, name="todo_item_create"),
    path("items/<int:id>/edit/", item_update, name="todo_item_update"),
]
